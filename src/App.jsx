import { useEffect, useState } from 'react'

function App() {


  const [ update , setUpdate] = useState(0);

  const [data,setData] = useState([]);

    useEffect(()=>{
      fetch('https://app-0nxa.onrender.com/').then(data => data.json()).then( data => setData(data))
    },[update])




    const addnew = (event) =>{
      event.preventDefault();
      const email = event.target.email.value;
      const pass = event.target.pass.value;
      const user = {email,pass}

      fetch('https://app-0nxa.onrender.com/' , {
        method:'POST' ,
        headers:{
          'Content-type' : 'application/json'
        },
        body : JSON.stringify(user)
      }).then(data => data.json()).then(data => {
        if(data.acknowledged == true){
          setUpdate(update+1)
        }
      })
    }




    const del =(data) =>{
      fetch('https://app-0nxa.onrender.com/',{
        method :'DELETE' ,
        headers : {
          'Content-type' : 'application/json'
        },
        body : JSON.stringify({data})
      }).then(data => data.json()).then(data => {
        if(data.deletedCount > 0 ){
          setUpdate(update+1)
        }
      })
    }

    const upt = (event,id) =>{
      event.preventDefault();
      const email = event.target.email.value
      const pass = event.target.pass.value
      const user = {id ,email ,pass}

      fetch('https://app-0nxa.onrender.com/' , {
        method :'PUT' ,
        headers : {'Content-type' :  'application/json'} ,
        body : JSON.stringify(user)
      }).then(data => data.json()).then(data =>{
        if(data.modifiedCount == 1){
          setUpdate(update+1)
          document.getElementById('edit').classList.toggle('hidden')
        }
      })
     
      
    }

  return (
    <>


    <div>
      <h1 className='text-3xl ml-20 mt-2'>{data.length}</h1>
      {
        data.map(data => 
        <div key={data._id} className='flex gap-3 space-y-3 items-center border border-blue-500 ml-10 mt-10'>
          <h1>Email : {data.email}</h1>
          <h1>Pass : {data.pass}</h1>
          <button className='bg-blue-500 hover:bg-blue-700 text-white font-bold rounded px-2' onClick={()=>{del(data._id)}}>Delete</button>
          <button className='bg-blue-500 hover:bg-blue-700 text-white font-bold rounded px-2' onClick={()=>{ document.getElementById('edit').classList.toggle('hidden')}}>Edit</button>
          <br />



          <div className='flex hidden' id='edit'>
            <form onSubmit={(event) => {upt(event,data._id)}}>

            
          <input type="text" name='email' placeholder='email' className='border border-black mr-2' defaultValue={data.email}/>
          <input type="text" name='pass' placeholder='email' className='border border-black mr-2' defaultValue={data.pass}/>
          <input type="submit" value="Update" className='bg-blue-500 hover:bg-blue-700 text-white font-bold rounded p-2 mt-3 ml-20 cursor-pointer'/>
          </form>
          </div>

        </div> )
      }




      <div className='mt-10'>
        <form className='ml-5' onSubmit={addnew}>
          <input type="text" name='email' placeholder='email' className='border border-black mr-2'/>
          <input type="text" name='pass' placeholder='pass' className='border border-black'/>
          <br />
          <input type="submit" value="Add new" className='bg-blue-500 hover:bg-blue-700 text-white font-bold rounded p-2 mt-3 ml-20 cursor-pointer'/>
        </form>
      </div>
      
    </div>
   
    </>
  )
}

export default App
